
var path = require('path');
var express = require('express');
var mysql = require('mysql');

var config = require('./config.json');
var connection = mysql.createConnection(config.mysql);

var app = express();
app.use(express.json());

// Statics
app.use(express.static('public/static'));

// Pages
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname+'/public/page/index.html'));
});

// API
app.get('/api/tasks', function(req, res) {
  connection.query('SELECT * FROM tasks', function (error, results, fields) {
    if (error) {
      res.status(404).send({data: []});
      console.error('Error retrieving the tasks');
      console.error(error);
    }

    res.send({
      data: results
    });
  });  
});

app.post('/api/tasks', function(req, res) {
  connection.query('INSERT INTO `todo`.`tasks` (`tasks`, `status`) VALUES (\'' + req.body.task + '\', \'pending\');', function (error, results) {
    if (error) {
      res.status(404);
      console.error('Error creating the tasks');
      console.error(error);
    }
    res.send('OK');
  });
});

app.put('/api/tasks', function(req, res) {
  connection.query('UPDATE `todo`.`tasks` SET `status` = \'complete\' WHERE (`id` = \'' + req.body.id + '\'); ', function (error, results) {
    if (error) {
      res.status(404);
      console.error('Error updating the tasks');
      console.error(error);
    }
    res.send('OK');
  });
});

// Start app
app.listen(config.express.port, function () {
  console.log('Example app listening on port '+ config.express.port + '.');
});
