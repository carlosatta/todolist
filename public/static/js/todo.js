var services = {
  tasks: {
    get: function() {
      return fetch('http://0.0.0.0:3000/api/tasks');
    },
    create: function(task) {
      return fetch('http://0.0.0.0:3000/api/tasks', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          task: task,
        }),
      });
    },
    update: function(id) {
      return fetch('http://0.0.0.0:3000/api/tasks', {
        method: 'put',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: id,
        }),
      });
    },  
    delete: function() {

    },
  },
};

init();

// Functions

function init() {
  updateLists();
}

function updateLists() {
  updateTasksList();
  updateDoneTasksList();
}

function updateTasksList() {
  var taskList = document.getElementById('tasks');
  taskList.innerHTML = '';

  services.tasks.get()
    .then(function(res) {
      return  res.json();
    })
    .then(function(tasks) {
      var html = ''
      tasks = tasks.data.filter(function(task) {
        return task.status === 'pending';
      });

      for(var task of tasks) {
        html += '<div class="col-10 mT-2">' + task.tasks + '</div><div class="col-2 mT-2 text-right"><a class="btn btn-success" href="#" onClick="completeTask(event, ' + task.id + ')">Done</a></div>';
      }
      taskList.innerHTML = html;
    })
    .catch(function(error) {
      console.error(error);
    });
}

function updateDoneTasksList() {
  var taskList = document.getElementById('taskDone');
  taskList.innerHTML = '';

  services.tasks.get()
    .then(function(res) {
      return  res.json();
    })
    .then(function(tasks) {
      var html = ''
      tasks = tasks.data.filter(function(task) {
        return task.status === 'complete';
      });

      for(var task of tasks) {
        html += '<div class="col-12 mT-2"><del>' + task.tasks + '</del></div>';
      }
      taskList.innerHTML = html;
    })
    .catch(function(error) {
      console.error(error);
    });
}


function createTask(e) {
  var task = document.getElementById('newTask').value;
  // document.getElementById('newTask').value = '';

  services.tasks.create(task)
    .then(function() {
      updateLists();
    })
    .catch(function(error) {
      console.error(error);
    });

}

function completeTask(e, id) {
  e.preventDefault();
  services.tasks.update(id)
    .then(function() {
      updateLists();
    })
    .catch(function(error) {
      console.error(error);
    });
}


function createAlert(type, message) {

}